/*
 * tab:2
 *
 * search_asm.S - Implementation of Assembly Recursive DFS
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice and the following
 * two paragraphs appear in all copies of this software.
 *
 * IN NO EVENT SHALL THE AUTHOR OR THE UNIVERSITY OF ILLINOIS BE LIABLE TO
 * ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 * DAMAGES ARISING OUT  OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF THE AUTHOR AND/OR THE UNIVERSITY OF ILLINOIS HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHOR AND THE UNIVERSITY OF ILLINOIS SPECIFICALLY DISCLAIM ANY
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND NEITHER THE AUTHOR NOR
 * THE UNIVERSITY OF ILLINOIS HAS ANY OBLIGATION TO PROVIDE MAINTENANCE,
 * SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 * Author:        Aamir Hasan
 * Version:       1
 * Creation Date: Fri Aug 30 2020
 * Filename:      search_asm.S
 * History:
 *    AS    1    Fri Aug 30 2020
 *        First written.
 */

.data
  KEY   = 0
  LEFT  = 4
  RIGHT = 8

.text
.global search_asm

# Search ASM (Assembly)
# Searches for an element in a BST
# Declaration is in search.h
#
# Registers:
#   eax - Return Value
# 
search_asm:

  pushl %ebp			#Push base pointer to stack
  movl %esp, %ebp  		#Move stack pointer to base pointer
  #--- YOUR CODE HERE ---

  #Stack breakdown...
  #%ebx <- root
  #%ecx <- key 
  #var1 == root.key
  subl $4, %esp  #Allocating space for one local variable
  pushl %ebx    #Saving the values of ebx and ecx
  pushl %ecx
  
  #Body
  mov 8(%ebp), %ebx     #Initializing parameters
  mov 12(%ebp), %ecx     
  
  cmp $0, %ebx        #Checking if root is null
  je RETURN_N1
  
  mov %ebx, -4(%ebp)  #Fetching the root->key
  mov (%ebx), %ebx
  cmp %ecx, %ebx   #Checking if root->key = key
  je RETURN_0
  
  cmpg %ecx, %ebx   #Checking if key < root->key
  je RETURN_LEFT
  
  cmpl %ecx, %ebx   #Checking if key > root->key
  je RETURN_RIGHT
  
RETURN_N1:
  mov $-1, %eax     #Moving -1 into return register
  jmp RETURN_GENERAL
  
RETURN_0:
  movl $0, %eax
  jmp RETURN_GENERAL

RETURN_LEFT:
  #Initializing caller stack
  push %ecx 
  movl -4(%ebp), %ebx      #Value located 4 bytes from the root pointer
  push 4(%ebx)
  call search_asm       #Assuming %eax will already contain the return 
  jmp RETURN_GENERAL

RETURN_RIGHT:
  push %ecx 
  movl -4(%ebp), %ebx      #Value located 4 bytes from the root pointer
  push 8(%ebx)      #Value located 8 bytes from the root pointer
  call search_asm       #Assuming %eax will already contain the return 
  jmp RETURN_GENERAL
  
RETURN_GENERAL:
  pop %ecx
  pop %ebx

#----------------------

  leave				#Stack cleanup
  ret				#Return to caller


