/*

 * tab:2
 *
 * mystery.c
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice and the following
 * two paragraphs appear in all copies of this software.
 *
 * IN NO EVENT SHALL THE AUTHOR OR THE UNIVERSITY OF ILLINOIS BE LIABLE TO
 * ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
 * DAMAGES ARISING OUT  OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION,
 * EVEN IF THE AUTHOR AND/OR THE UNIVERSITY OF ILLINOIS HAS BEEN ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE AUTHOR AND THE UNIVERSITY OF ILLINOIS SPECIFICALLY DISCLAIM ANY
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE
 * PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND NEITHER THE AUTHOR NOR
 * THE UNIVERSITY OF ILLINOIS HAS ANY OBLIGATION TO PROVIDE MAINTENANCE,
 * SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 * Author:        Aamir Hasan
 * Version:       1
 * Creation Date: Sun Aug 30 2020
 * Filename:      mystery.c
 * History:
 *    AH    1    Sun Aug 30 2020
 *        First written.
 */

#include "mystery.h"

int32_t mystery_c(int32_t x, int32_t* y) {
  //------- YOUR CODE HERE -------
int i;
int out = 0;
//Judge if the input is available.
if (x<=0){
	return -1;
}
//If it's available:
else{
//The op_loop
	while(x > 0){
			*(y + out) = x;
				out+=1;
			//The op_loop will end here if x finally becomes 1
			if(x == 1){
					break;
				}
			//Here the op_loop will go into a desision
			else{
			//Let i = x and give it a right shift for 1 bit
					i = x;
					i = i >> 1;
					//If x is odd, move to op2
					if(x%2 != 0){
						i = x;
						i = 3*i;
						i += 1;
						x = i;						
						}
					//If x is even, move to op1
					else{
						x = i;
						}

				}
		}
}
//Return out
  return out;
  //------------------------------
}
